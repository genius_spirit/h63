import React from 'react';
import Input, { InputLabel } from 'material-ui/Input';
import './AddPanel.css';
import FormControl from "material-ui/es/Form/FormControl";
import Button from 'material-ui/Button';

const AddPanel = ({clicked, change, text}) => {
    return(
    <div className="AddPanel">
      <FormControl>
        <InputLabel htmlFor="name-input">Movie</InputLabel>
        <Input id="name-input" value={text} onChange={change}/>
      </FormControl>
      <div className='btn-wrapper'>
        <Button variant="raised" size="small" className='btn' onClick={clicked}>Add</Button>
      </div>

    </div>
  )
};

export default AddPanel;