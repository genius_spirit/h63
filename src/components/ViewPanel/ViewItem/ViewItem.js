import React, { Component } from 'react';
import Input from "material-ui/es/Input/Input";

class ViewItem extends Component {

  shouldComponentUpdate (nextProps) {
    return nextProps.movieTitle !== this.props.movieTitle;
  }

  render() {
    return(
      <Input className="ViewPanel-input" value={this.props.movieTitle}
             onChange={(event) => this.props.changeMovie(event, this.props.movieId)} />
    )
  }
}

export default ViewItem;