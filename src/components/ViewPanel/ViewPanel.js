import React, {Component, Fragment} from 'react';
import './ViewPanel.css';
import ViewItem from "./ViewItem/ViewItem";

class ViewPanel extends Component{
  render() {
    return (
      <Fragment>
        <div className='title'>My watch list:</div>
        <div className="ViewPanel">
          { Object.keys(this.props.movies).map(itemId => {
            return (
              <div key={itemId} className='ViewPanel-item'>
                <ViewItem changeMovie={this.props.changeMovie} movieTitle={this.props.movies[itemId].movie} movieId={itemId}/>
                <i className="material-icons ViewPanel-save"
                   onClick={() => this.props.edited(itemId)}>save</i>
                <i className="material-icons ViewPanel-remove"
                   onClick={() => this.props.remove(itemId)}>delete</i>
              </div>
            )
          })
          }
        </div>
      </Fragment>
    )
  }
}

export default ViewPanel;