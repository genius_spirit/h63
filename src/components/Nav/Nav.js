import React from 'react';
import {Link} from "react-router-dom";
import './Nav.css';
import {Tooltip} from "material-ui";

const Nav = () => {
  return(
    <div className='container'>
      <div className="homepage">
        <Link to="/">
          <i className="material-icons icon md-30">home</i>
          <h3 className="title-h3 ">Homepage</h3>
        </Link>
      </div>
      <div className="todoList">
        <Tooltip title="To-do list">
          <Link to="/todo">
            <i className="material-icons icon md-36">playlist_add_check</i>
            <h3 className="title-h3 ">To-do app</h3>
          </Link>
        </Tooltip>
      </div>
      <div className="movieList">
        <Tooltip title="Favorite movie list">
          <Link to="/movies">
            <i className="material-icons icon md-30">add_to_queue</i>
            <h3 className="title-h3 ">Movies app</h3>
          </Link>
        </Tooltip>
      </div>
    </div>
  )
};

export default Nav;