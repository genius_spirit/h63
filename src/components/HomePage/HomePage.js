import React from 'react';
import './HomePage.css';
import {Link} from "react-router-dom";

const HomePage = () => {
  return(
    <div className="homepage-wrapper">
      <h3>My to-do list</h3>
      <Link to="/todo"><div className="homepage-todo" /></Link>
      <h3>My movie list</h3>
      <Link to="/movies"><div className="homepage-movie" /></Link>
    </div>
  )
};

export default HomePage;