import React, {Fragment} from 'react';
import {Switch, Route} from "react-router-dom";
import TodoApp from "./containers/TodoApp/TodoApp";
import MoviesApp from "./containers/MoviesApp/MoviesApp";
import './App.css';
import Nav from "./components/Nav/Nav";
import HomePage from "./components/HomePage/HomePage";

const App = () => {
  return (
    <Fragment>
      <Nav/>
      <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route path="/todo" component={TodoApp}/>
        <Route path="/movies" component={MoviesApp}/>
      </Switch>
    </Fragment>
  );
};

export default App;
