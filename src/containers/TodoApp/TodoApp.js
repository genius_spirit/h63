import React, {Component} from 'react';
import AddTaskForm from "../../components/AddTaskForm/AddTaskForm";
import Tasks from "../../components/Tasks/Tasks";
import axios from 'axios';
import './TodoApp.css';
import {Tooltip} from "material-ui";
import Spinner from "../../components/Spinner/Spinner";

class TodoApp extends Component {

  state = {
    tasks: [],
    currentTask: '',
    loading: false,
    showForm: false,
  };

  componentDidMount() {
    this.setState({loading: true});
    this.getTasks();
  }

  getTasks = () => {
    axios.get('/todo.json').then(response => {
      // const notes = [];
      // for (let key in response.data) {
      //   notes.push({...response.data[key], id: key})
      // }
      response.data ? this.setState({tasks: response.data}) : this.setState({tasks: []});
    }).finally(() => {
      this.setState({loading: false});
    })

  };

  currentTaskChange = (event) => {
    const text = event.target.value;
    this.setState({currentTask: text});
  };

  createTaskHandler = () => {
    if (this.state.currentTask.length > 0) {
      this.setState({loading: true});
      const item = { task: this.state.currentTask, checked: false};
      axios.post('/todo.json', item).then(() => {
        this.getTasks();
        this.setState({currentTask: ''});
      });
    } else alert('You have not text in input');

  };

  deleteTaskHandler = (id) => {
    this.setState({loading: true});
    axios.delete(`/todo/${id}.json`).then(() => {
      this.getTasks();
    });
    this.setState({loading: false});
  };

  isShowForm = () => {
    this.setState({showForm: !this.state.showForm})
  };

  checkedTaskHandler = (id) => {
    const tasksCopy = {...this.state.tasks};
    const task = {...this.state.tasks[id]};
    task.checked = !task.checked;
    tasksCopy[id] = task;
    this.setState({tasks: tasksCopy});
  };

  render() {
    return (
      !this.state.loading ?
      <div className="todo-form">
        <h3>My to-do list:</h3>
        { this.state.showForm
          ? <AddTaskForm
            change={(event) => this.currentTaskChange(event)}
            clicked={this.createTaskHandler}
            value={this.state.currentTask} />
          : <Tooltip title="Add new post">
              <i className="material-icons md-24 add" onClick={this.isShowForm}>library_add</i>
            </Tooltip>
        }
        { Object.keys(this.state.tasks).map((taskId) => {
          return <Tasks
            text={this.state.tasks[taskId].task}
            key={taskId}
            isChecked={this.state.tasks[taskId].checked}
            checked={() => this.checkedTaskHandler(taskId)}
            removed={() => this.deleteTaskHandler(taskId)}/>
        })}
      </div> : <Spinner/>
    );
  }
}

export default TodoApp;