import React, { Component } from 'react';
import AddPanel from "../../components/AddPanel/AddPanel";
import ViewPanel from "../../components/ViewPanel/ViewPanel";
import axios from 'axios';
import './MovieApp.css';
import Spinner from "../../components/Spinner/Spinner";


class MoviesApp extends Component {

  state = {
    movies: [],
    currentMovie: '',
    loading: false
  };

  handlerChangeCurrentMovie = (event) => {
    let currentText = event.target.value;
    this.setState({currentMovie: currentText});
  };

  getMovies = () => {
    axios.get('/movies.json').then(response => {
      response.data ? this.setState({movies: response.data}) : this.setState({movies: []});
    }).finally(() => {
      this.setState({loading: false});
    })
  };

  handlerAddNewMovie = () => {
    this.setState({loading: true});
    if (this.state.currentMovie.length > 0) {
      const movie = {movie: this.state.currentMovie};
      axios.post('/movies.json', movie).then(() => {
        this.getMovies();
      })
    }
  };

  handlerChangeMovieInList = (event, id) => {
    const moviesCopy = {...this.state.movies};
    const itemCopy = {...this.state.movies[id]};
    itemCopy.movie = event.target.value;
    moviesCopy[id] = itemCopy;
    this.setState({movies: moviesCopy});
  };

  handlerRemoveMovieFromList = (id) => {
    this.setState({loading: true});
    axios.delete(`/movies/${id}.json`).then(() => {
      this.getMovies();
    })
  };

  editMovieTitleHandler = (id) => {
    this.setState({loading: true});
    axios.patch(`/movies/${id}.json`, this.state.movies[id]).then(() => {
      this.setState({loading: false});
    })
  };

  componentDidMount() {
    this.setState({loading: true});
    this.getMovies();
  }

  render() {
    return (
      this.state.loading
        ? <Spinner/>
        : <div className='movieApp'>
          <AddPanel
            text={this.state.currentMovie}
            clicked={this.handlerAddNewMovie}
            change={(event) => this.handlerChangeCurrentMovie(event)}/>
          <ViewPanel
            movies={this.state.movies}
            changeMovie={this.handlerChangeMovieInList}
            remove={this.handlerRemoveMovieFromList}
            edited={this.editMovieTitleHandler}/>
        </div>
    );
  }
}

export default MoviesApp;